#!/bin/bash
url=$1
status=$2

HTTPD="000"
until [ "$HTTPD" == "${status}" ]; do
    echo "$HTTPD <> ${status}"
    sleep 3
    HTTPD=`curl -I -A "Web Check" -sL --connect-timeout 3 -w "%{http_code}\n" "${url}" -o /dev/null`
done